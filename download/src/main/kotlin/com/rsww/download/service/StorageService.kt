package com.rsww.download.service

import com.rsww.download.entity.FileEntity
import com.rsww.download.repository.FileRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.stream.Stream

@Service
class StorageService @Autowired constructor(
    private val fileRepository: FileRepository
) {

  fun getFile(id: Long): FileEntity {
    return fileRepository.findById(id).get()
  }

  fun getAllFiles(templates: Boolean): Stream<FileEntity> {
    return fileRepository.findByIsTemplate(templates).stream()
  }
}
