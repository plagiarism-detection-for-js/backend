package com.rsww.download.controller

import com.rsww.download.dto.FileDto
import com.rsww.download.service.StorageService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.stream.Collectors
import javax.servlet.http.HttpServletRequest


@RestController
@RequestMapping("/download")
class DownloadController @Autowired constructor(
    private val storageService: StorageService
) {

  private val logger = LoggerFactory.getLogger(DownloadController::class.java)

  @GetMapping("/file")
  fun getFiles(): ResponseEntity<List<FileDto>> {
    val files =
        storageService
            .getAllFiles(false)
            .map { file ->
              FileDto(
                  id = file.id,
                  name = file.name
              )
            }
            .collect(Collectors.toList())
    return ResponseEntity(files, HttpStatus.OK)
  }

  @GetMapping("/template")
  fun getTemplates(): ResponseEntity<List<FileDto>> {
    val files =
        storageService
            .getAllFiles(true)
            .map { file ->
              FileDto(
                  id = file.id,
                  name = file.name
              )
            }
            .collect(Collectors.toList())
    return ResponseEntity(files, HttpStatus.OK)
  }

  @GetMapping("/file/{id}")
  fun getFile(@PathVariable id: Long): ResponseEntity<ByteArray> {
    val file = storageService.getFile(id)
    return ResponseEntity
        .ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, """attachment; filename="${file.name}"""")
        .body(file.data)
  }
}
