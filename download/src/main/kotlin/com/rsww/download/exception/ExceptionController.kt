package com.rsww.download.exception

import com.rsww.shared.exception.ErrorResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.multipart.MaxUploadSizeExceededException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class ExceptionController : ResponseEntityExceptionHandler() {

  @ExceptionHandler(MaxUploadSizeExceededException::class)
  fun handleMaxSizeException(exc: MaxUploadSizeExceededException?): ResponseEntity<ErrorResponse> {
    return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(ErrorResponse("File too large!"))
  }
}
