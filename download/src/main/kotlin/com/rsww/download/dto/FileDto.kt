package com.rsww.download.dto

data class FileDto(

    var id: Long = 0L,

    var name: String = ""
)
