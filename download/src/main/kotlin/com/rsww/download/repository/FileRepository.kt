package com.rsww.download.repository

import com.rsww.download.entity.FileEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FileRepository : JpaRepository<FileEntity, Long> {

  fun findByIsTemplate(isTemplate: Boolean): List<FileEntity>
}
