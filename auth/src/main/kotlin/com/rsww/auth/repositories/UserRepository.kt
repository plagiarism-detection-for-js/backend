package com.rsww.auth.repositories

import com.rsww.auth.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<UserEntity, String> {

  fun existsByUsername(username: String): Boolean

  fun findByUsername(username: String): UserEntity?
}
