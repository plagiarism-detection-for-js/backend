package com.rsww.auth

import org.modelmapper.ModelMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean

@SpringBootApplication
@EnableEurekaClient
class AuthApplication {

  @Bean
  fun modelMapper(): ModelMapper =
    ModelMapper()
}

fun main(args: Array<String>) {
  runApplication<AuthApplication>(*args)
}
