package com.rsww.auth.controllers

import com.rsww.auth.dto.UserDto
import com.rsww.auth.entity.UserEntity
import com.rsww.auth.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.modelmapper.ModelMapper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/auth")
class AuthController @Autowired constructor(
    private val userService: UserService,
    private val modelMapper: ModelMapper
) {

  @PostMapping("/login")
  fun login(@RequestBody dto: UserDto): ResponseEntity<String> {
    val token = userService.login(dto.username, dto.password)
    return ResponseEntity(token, HttpStatus.OK)
  }

  @PostMapping("/register")
  fun register(@RequestBody dto: UserDto): ResponseEntity<String> {

    val token = userService.register(modelMapper.map(dto, UserEntity::class.java))
    return ResponseEntity(token, HttpStatus.OK)
  }

  @GetMapping("/refresh")
  @ResponseStatus(HttpStatus.OK)
  fun refresh(req: HttpServletRequest) =
      userService.refresh(req.remoteUser)
}
