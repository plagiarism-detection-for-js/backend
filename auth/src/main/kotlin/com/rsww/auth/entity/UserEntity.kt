package com.rsww.auth.entity

import javax.persistence.*

@Entity
class UserEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0L,

    @Column(unique = true, nullable = false)
    var username: String = "",

    var password: String = "",
)
