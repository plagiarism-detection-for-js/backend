package com.rsww.auth.dto

data class UserDto(

  var username: String,

  var password: String
)
