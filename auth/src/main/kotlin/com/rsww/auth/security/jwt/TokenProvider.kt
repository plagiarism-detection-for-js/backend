package com.rsww.auth.security.jwt

import com.rsww.auth.exceptions.CustomException
import com.rsww.auth.security.AuthConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.http.HttpStatus
import io.jsonwebtoken.JwtException
import javax.servlet.http.HttpServletRequest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

@Component
class TokenProvider @Autowired constructor(
    private val userDetails: UserDetailsImpl
) {

  private val expirationTime = AuthConfig.EXPIRATION_TIME

  private var secret: String = Base64.getEncoder().encodeToString(AuthConfig.SECRET.toByteArray())

  fun createToken(username: String): String {
    val claims: Claims = Jwts.claims().setSubject(username)
    claims["auth"] = listOf(
        SimpleGrantedAuthority(
            AuthConfig.DEFAULT_ROLE
        ))
        .filter(Objects::nonNull)
    val now = Date()
    val validity = Date(now.time + expirationTime)
    return Jwts.builder()
        .setClaims(claims)
        .setIssuedAt(now)
        .setExpiration(validity)
        .signWith(SignatureAlgorithm.HS256, secret)
        .compact()
  }

  fun getAuthentication(token: String): Authentication? {
    val userDetails: UserDetails = userDetails.loadUserByUsername(getUsername(token))
    return UsernamePasswordAuthenticationToken(
        userDetails, "", userDetails.authorities)
  }

  fun getUsername(token: String): String =
      Jwts.parser().setSigningKey(secret).parseClaimsJws(token).body.subject

  fun resolveToken(req: HttpServletRequest): String? {
    val bearerToken = req.getHeader("Authorization")
    return if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
      bearerToken.substring(7)
    } else null
  }

  fun validateToken(token: String): Boolean {
    return try {
      Jwts.parser().setSigningKey(secret).parseClaimsJws(token)
      true
    } catch (e: JwtException) {
      throw CustomException("Token is invalid or has expired, please log in", HttpStatus.UNAUTHORIZED)
    } catch (e: IllegalArgumentException) {
      throw CustomException("Token is invalid or has expired, please log in", HttpStatus.UNAUTHORIZED)
    }
  }
}