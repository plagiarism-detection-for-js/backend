package com.rsww.auth.security.jwt

import com.rsww.auth.exceptions.CustomException
import org.springframework.security.core.Authentication
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.core.context.SecurityContextHolder

class TokenFilter(
  private val tokenProvider: TokenProvider
) : OncePerRequestFilter() {

  override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {

    val token: String? = tokenProvider.resolveToken(request)
    try {
      if (token != null && tokenProvider.validateToken(token)) {

        val auth: Authentication? = tokenProvider.getAuthentication(token)

        SecurityContextHolder.getContext().authentication = auth
      }
    } catch (ex: CustomException) {

      SecurityContextHolder.clearContext()
      response.sendError(ex.httpStatus.value(), ex.message)
      return
    }

    chain.doFilter(request, response)
  }
}
