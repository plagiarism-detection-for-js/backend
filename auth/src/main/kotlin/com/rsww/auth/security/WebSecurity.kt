package com.rsww.auth.security

import com.rsww.auth.security.jwt.TokenFilterConfigurer
import com.rsww.auth.security.jwt.TokenProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.authentication.AuthenticationManager

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

import org.springframework.security.crypto.password.PasswordEncoder


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurity @Autowired constructor(
  private val tokenProvider: TokenProvider
) : WebSecurityConfigurerAdapter() {

  override fun configure(http: HttpSecurity?) {

    http?.csrf()?.disable()

    http?.sessionManagement()?.sessionCreationPolicy(SessionCreationPolicy.STATELESS)

    http?.authorizeRequests()
      ?.antMatchers("/auth/register")?.permitAll()
      ?.antMatchers("/auth/login")?.permitAll()
      ?.anyRequest()?.authenticated()

    http?.exceptionHandling()?.accessDeniedHandler { _, response, exception ->
      response.sendError(
        403,
        exception.toString()
      )
    }

    http?.apply(TokenFilterConfigurer(tokenProvider))
  }

  override fun configure(web: WebSecurity?) {

    web?.ignoring()?.antMatchers("/v2/api-docs")//
      ?.antMatchers("/swagger-resources/**")//
      ?.antMatchers("/swagger-ui/**")//
      ?.antMatchers("/configuration/**")//
      ?.antMatchers("/webjars/**")//
      ?.antMatchers("/public")
  }

  @Bean
  fun passwordEncoder(): PasswordEncoder =
    BCryptPasswordEncoder(12)

  @Bean
  override fun authenticationManagerBean(): AuthenticationManager =
    super.authenticationManagerBean()

}
