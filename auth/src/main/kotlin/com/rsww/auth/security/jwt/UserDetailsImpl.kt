package com.rsww.auth.security.jwt

import com.rsww.auth.repositories.UserRepository
import com.rsww.auth.security.AuthConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserDetailsImpl @Autowired constructor(
    private val userRepository: UserRepository
) : UserDetailsService {

  override fun loadUserByUsername(username: String): UserDetails {

    val entity = userRepository.findByUsername(username)

    entity ?: throw UsernameNotFoundException("User '$username' not found")

    return User
      .withUsername(username)
      .password(entity.password)
      .authorities(AuthConfig.DEFAULT_ROLE)
      .accountExpired(false)
      .accountLocked(false)
      .credentialsExpired(false)
      .disabled(false)
      .build()
  }

}
