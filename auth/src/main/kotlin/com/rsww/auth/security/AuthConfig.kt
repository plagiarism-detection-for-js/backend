package com.rsww.auth.security

object AuthConfig {

  const val SECRET = "eyJhc2Rhc2QiOiJhc2RhcyIsImFzZGFzZGFzIjoiYWFzZGRzYWRhc2QiLCJhbGciOiJIUzI1NiJ9.eyJhc2Rhc2RhcyI6ImFzZGFzZGFzZGFzZGEiLCJhc2Rhc2Rhc2Rhc2QiOiJhc2Rhc2Rhc2Rhc2QiLCJhc2Rhc2Rhc2RzYSI6ImFzZGFzZHNhc3NhZCJ9.-6zU68dwjspNJebyj_c8OtyLAB3XarUHCU5H7QIVlWA"
  const val EXPIRATION_TIME: Long = 1_800_000
  const val DEFAULT_ROLE = "DEFAULT_ROLE"
}
