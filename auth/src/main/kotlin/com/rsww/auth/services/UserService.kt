package com.rsww.auth.services

import com.rsww.auth.entity.UserEntity
import com.rsww.auth.exceptions.CustomException
import com.rsww.auth.repositories.UserRepository
import com.rsww.auth.security.AuthConfig
import com.rsww.auth.security.jwt.TokenProvider
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException

@Service
class UserService(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder,
    private val tokenProvider: TokenProvider,
    private val authenticationManager: AuthenticationManager
) {

  fun login(username: String, password: String): String =
      try {
        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username, password))
        tokenProvider.createToken(username)
      } catch (e: AuthenticationException) {
        throw CustomException("Invalid credentials", HttpStatus.UNPROCESSABLE_ENTITY)
      }

  fun register(user: UserEntity): String {
    return if (!userRepository.existsByUsername(user.username)) {
      user.password = passwordEncoder.encode(user.password)
      userRepository.save<UserEntity>(user)
      tokenProvider.createToken(user.username)
    } else {
      throw CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY)
    }
  }

  fun refresh(username: String): String {
    return tokenProvider.createToken(username)
  }
}
