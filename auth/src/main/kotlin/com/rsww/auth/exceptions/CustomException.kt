package com.rsww.auth.exceptions

import org.springframework.http.HttpStatus


data class CustomException(

  override val message: String,

  val httpStatus: HttpStatus

) : RuntimeException()
