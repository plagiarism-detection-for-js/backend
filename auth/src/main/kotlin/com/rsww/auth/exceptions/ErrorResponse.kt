package com.rsww.auth.exceptions

data class ErrorResponse(

  var message: String,

  var timestamp: Long = System.currentTimeMillis()
)