package com.rsww.auth.exceptions

import org.springframework.boot.web.error.ErrorAttributeOptions
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.boot.web.servlet.error.ErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletResponse
import org.springframework.web.context.request.WebRequest
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.lang.Exception
import java.util.logging.Level
import java.util.logging.Logger

@RestControllerAdvice
class ExceptionController {

  private val logger = Logger.getGlobal()

  @Bean
  fun errorAttributes(): ErrorAttributes =
      object : DefaultErrorAttributes() {
        override fun getErrorAttributes(webRequest: WebRequest?, includeStackTrace: Boolean): Map<String, Any> {
          val options = ErrorAttributeOptions.defaults().including(ErrorAttributeOptions.Include.STACK_TRACE)
          val errorAttributes: MutableMap<String, Any> = super.getErrorAttributes(webRequest, options)
          errorAttributes.remove("exception")
          return errorAttributes
        }
      }

  @ExceptionHandler(CustomException::class)
  fun handleCustomException(res: HttpServletResponse, ex: CustomException): ResponseEntity<ErrorResponse> {
    logger.log(Level.WARNING, ex.message)

    return ResponseEntity(
        ErrorResponse(ex.message),
        ex.httpStatus
    )
  }

  @ExceptionHandler(AccessDeniedException::class)
  fun handleAccessDeniedException(res: HttpServletResponse, ex: AccessDeniedException): ResponseEntity<ErrorResponse> {
    logger.log(Level.WARNING, ex.message)

    return ResponseEntity(
        ErrorResponse(ex.message.orEmpty()),
        HttpStatus.FORBIDDEN
    )
  }

  @ExceptionHandler(Exception::class)
  fun handleException(res: HttpServletResponse, ex: Exception): ResponseEntity<ErrorResponse> {

    return ResponseEntity(
        ErrorResponse(ex.message.orEmpty()),
        HttpStatus.NOT_FOUND
    )
  }
}
