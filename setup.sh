docker build -t rsww-apigateway apigateway/
docker build -t rsww-command command/
docker build -t rsww-discovery discovery/
docker build -t rsww-download download/
docker build -t rsww-auth auth/
docker build -t rsww-query query/
docker build -t rsww-upload upload/
