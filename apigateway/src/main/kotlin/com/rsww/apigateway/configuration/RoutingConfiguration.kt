package com.rsww.apigateway.configuration

import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod

@Configuration
class RoutingConfiguration {

  @Bean
  fun routes(builder: RouteLocatorBuilder): RouteLocator =
      builder
          .routes()
          .route {
            it
                .path("/document/**")
                .and()
                .method(HttpMethod.GET)
                .uri("lb://query")
          }
          .route {
            it
                .path("/document/**")
                .and()
                .method(HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PUT)
                .uri("lb://command")
          }
          .route {
            it
                .path("/upload/**")
                .and()
                .method(HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PUT, HttpMethod.PATCH)
                .uri("lb://upload")
          }
          .route {
            it
                .path("/download/**")
                .and()
                .method(HttpMethod.GET)
                .uri("lb://download")
          }
          .route {
            it
                .path("/auth/**")
                .uri("lb://auth")
          }
          .build()
}
