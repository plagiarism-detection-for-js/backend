package com.rsww.shared.entity

import com.rsww.shared.utils.AnalysisStatus
import javax.persistence.*

@Entity
@Table(name = "analyses")
data class AnalysisEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0L,

    var fileId: Long = 0L,

    var fileName: String = "",

    @AnalysisStatus
    var status: String = AnalysisStatus.PENDING
)
