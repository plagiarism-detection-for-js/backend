package com.rsww.shared.exception

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.lang.Exception
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.http.HttpServletResponse

@RestControllerAdvice
class ExceptionController {

  private val logger = Logger.getGlobal()

  @ExceptionHandler(CustomException::class)
  fun handleCustomException(res: HttpServletResponse, ex: CustomException): ResponseEntity<ErrorResponse> {
    logger.log(Level.WARNING, ex.message)

    return ResponseEntity(
        ErrorResponse(ex.message),
        HttpStatus.CONFLICT
    )
  }

  @ExceptionHandler(AccessDeniedException::class)
  fun handleAccessDeniedException(res: HttpServletResponse, ex: AccessDeniedException): ResponseEntity<ErrorResponse> {
    logger.log(Level.WARNING, ex.message)

    return ResponseEntity(
        ErrorResponse(ex.message.orEmpty()),
        HttpStatus.FORBIDDEN
    )
  }

  @ExceptionHandler(Exception::class)
  fun handleException(res: HttpServletResponse, ex: Exception): ResponseEntity<ErrorResponse> {

    return ResponseEntity(
        ErrorResponse(ex.message.orEmpty()),
        HttpStatus.NOT_FOUND
    )
  }
}
