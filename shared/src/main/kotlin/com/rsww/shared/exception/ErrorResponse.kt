package com.rsww.shared.exception

data class ErrorResponse(

  var message: String,

  var timestamp: Long = System.currentTimeMillis()
)