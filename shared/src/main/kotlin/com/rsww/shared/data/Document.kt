package com.rsww.shared.data

data class Document(

  var id: Long,

  var name: String
)
