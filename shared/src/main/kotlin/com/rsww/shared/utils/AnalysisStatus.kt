package com.rsww.shared.utils

@Retention(AnnotationRetention.SOURCE)
annotation class AnalysisStatus {

  companion object {

    const val PENDING = "PENDING"

    const val IN_PROGRESS = "IN_PROGRESS"

    const val DONE = "DONE"
  }
}
