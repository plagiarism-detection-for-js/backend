package com.rsww.command.configuration

import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository
import org.springframework.boot.actuate.trace.http.HttpTraceRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class HttpTraceActuatorConfiguration {
  @Bean
  fun httpTraceRepository(): HttpTraceRepository =
    InMemoryHttpTraceRepository()
}
