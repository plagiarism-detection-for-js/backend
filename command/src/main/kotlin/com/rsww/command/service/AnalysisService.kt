package com.rsww.command.service

import com.rsww.command.repository.AnalysisRepository
import com.rsww.shared.entity.AnalysisEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AnalysisService @Autowired constructor(
    private val analysisRepository: AnalysisRepository
) {

  fun startAnalysis(fileId: Long) =
      analysisRepository.save(
          AnalysisEntity(
              fileId = fileId,
              fileName = "placeholder.js"
          )
      )

}
