package com.rsww.command

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
@EntityScan(basePackages = ["com.rsww.shared.entity"])
class CommandApplication

fun main(args: Array<String>) {
	runApplication<CommandApplication>(*args)
}
