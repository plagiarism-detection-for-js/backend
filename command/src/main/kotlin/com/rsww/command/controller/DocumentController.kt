package com.rsww.command.controller

import com.rsww.command.service.AnalysisService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/document")
class DocumentController @Autowired constructor(
    private val analysisService: AnalysisService
) {

  private val logger = LoggerFactory.getLogger(DocumentController::class.java)

  @PostMapping("/start-analysis/{fileId}")
  private fun startAnalysis(@PathVariable("fileId") fileId: Long): ResponseEntity<String> =
    try {
      analysisService.startAnalysis(fileId)
      ResponseEntity("Analysis started", HttpStatus.OK)
    } catch (e: Exception) {
      ResponseEntity(e.message, HttpStatus.CONFLICT)
    }
}
