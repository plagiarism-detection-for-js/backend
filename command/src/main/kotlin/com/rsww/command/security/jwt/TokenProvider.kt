package com.rsww.command.security.jwt

import com.rsww.shared.exception.CustomException
import com.rsww.shared.security.SecretConfig
import org.springframework.stereotype.Component
import java.util.*
import io.jsonwebtoken.Jwts
import org.springframework.http.HttpStatus
import io.jsonwebtoken.JwtException
import javax.servlet.http.HttpServletRequest

@Component
class TokenProvider {

  private var secret: String = Base64.getEncoder().encodeToString(SecretConfig.SECRET.toByteArray())

  fun getUsername(token: String): String =
      Jwts.parser().setSigningKey(secret).parseClaimsJws(token).body.subject

  fun resolveToken(req: HttpServletRequest): String? {
    val bearerToken = req.getHeader("Authorization")
    return if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
      bearerToken.substring(7)
    } else null
  }

  fun validateToken(token: String): Boolean {
    return try {
      Jwts.parser().setSigningKey(secret).parseClaimsJws(token)
      true
    } catch (e: JwtException) {
      throw CustomException("Token is invalid or has expired, please log in", HttpStatus.UNAUTHORIZED)
    } catch (e: IllegalArgumentException) {
      throw CustomException("Token is invalid or has expired, please log in", HttpStatus.UNAUTHORIZED)
    }
  }
}