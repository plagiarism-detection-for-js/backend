package com.rsww.query.controller

import com.rsww.query.dto.AnalysisDto
import com.rsww.query.dto.ReportDto
import com.rsww.query.service.AnalysisService
import com.rsww.query.service.ReportService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/document")
class DocumentController @Autowired constructor(
    private val analysisService: AnalysisService,
    private val reportService: ReportService
) {

  private val logger = LoggerFactory.getLogger(DocumentController::class.java)

  @GetMapping("/report")
  fun getDocument(): ResponseEntity<List<ReportDto>> =
    ResponseEntity(
        reportService.getMockedReports(),
        HttpStatus.OK
    )

  @GetMapping("/analysis")
  fun getAnalysis(): ResponseEntity<List<AnalysisDto>> =
      try {
        val analyses = analysisService.getAnalysisList()
        ResponseEntity(
            analyses,
            HttpStatus.OK
        )
      } catch (e: Exception) {
        ResponseEntity(
            HttpStatus.NOT_FOUND
        )
      }
}
