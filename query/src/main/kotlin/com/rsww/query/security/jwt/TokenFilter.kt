package com.rsww.query.security.jwt

import com.rsww.shared.exception.CustomException
import com.rsww.shared.security.SecretConfig
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.core.context.SecurityContextHolder

class TokenFilter(
  private val tokenProvider: TokenProvider
) : OncePerRequestFilter() {

  override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {

    val token: String? = tokenProvider.resolveToken(request)
    try {
      if (token != null && tokenProvider.validateToken(token)) {

        val auth = UsernamePasswordAuthenticationToken(
            tokenProvider.getUsername(token),
            "",
            listOf(SimpleGrantedAuthority(
                SecretConfig.DEFAULT_ROLE
            ))
        )

        SecurityContextHolder.getContext().authentication = auth
      }
    } catch (ex: CustomException) {

      SecurityContextHolder.clearContext()
      response.sendError(ex.httpStatus.value(), ex.message)
      return
    }

    chain.doFilter(request, response)
  }
}
