package com.rsww.query.repository

import com.rsww.shared.entity.AnalysisEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AnalysisRepository: JpaRepository<AnalysisEntity, Long>
