package com.rsww.query.service

import com.rsww.query.dto.AnalysisDto
import com.rsww.query.repository.AnalysisRepository
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AnalysisService @Autowired constructor(
    private val analysisRepository: AnalysisRepository
) {

  fun getAnalysisList(): List<AnalysisDto> =
      analysisRepository
          .findAll()
          .map { ModelMapper().map(it, AnalysisDto::class.java) }
}
