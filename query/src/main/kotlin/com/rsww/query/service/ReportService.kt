package com.rsww.query.service

import com.rsww.query.dto.ReportDto
import com.rsww.query.mock.MockedReports
import org.springframework.stereotype.Service

@Service
class ReportService {

  fun getMockedReports(): List<ReportDto> =
      MockedReports.all()
}
