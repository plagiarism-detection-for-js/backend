package com.rsww.query.mock

import com.rsww.query.dto.ReportDto

object MockedReports {

  fun all(): List<ReportDto> =
      listOf(
          ReportDto(0L, 0L, 2, 0L, "testFile1.js"),
          ReportDto(1L, 1L, 4, 1L, "testFile2.js"),
          ReportDto(2L, 2L, 15, 2L, "testFile3.js"),
          ReportDto(3L, 3L, 2, 3L, "testFile4.js"),
          ReportDto(4L, 4L, 5, 4L, "testFile5.js"),
          ReportDto(5L, 5L, 6, 5L, "testFile6.js"),
          ReportDto(6L, 6L, 68, 6L, "testFile7.js"),
          ReportDto(7L, 7L, 1, 7L, "testFile8.js"),
          ReportDto(8L, 8L, 12, 8L, "testFile9.js"),
          ReportDto(9L, 9L, 3, 9L, "testFile10.js"),
          ReportDto(10L, 10L, 14, 10L, "testFile11.js"),
          ReportDto(11L, 11L, 2, 11L, "testFile12.js"),
          ReportDto(12L, 12L, 0, 12L, "testFile13.js"),
          ReportDto(13L, 13L, 0, 13L, "testFile14.js"),
          ReportDto(14L, 14L, 1, 14L, "testFile15.js"),
          ReportDto(15L, 15L, 1, 15L, "testFile16.js"),
          ReportDto(16L, 16L, 2, 16L, "testFile17.js"),
          ReportDto(17L, 17L, 1, 17L, "testFile18.js"),
          ReportDto(18L, 18L, 1, 18L, "testFile19.js"),
          ReportDto(19L, 19L, 0, 19L, "testFile20.js")
      )
}
