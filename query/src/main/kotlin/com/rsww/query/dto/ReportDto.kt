package com.rsww.query.dto

data class ReportDto(

    val id: Long = 0L,

    val analysisId: Long = 0L,

    val result: Int = 0,

    val fileId: Long = 0L,

    val fileName: String = "",

    val date: Long = System.currentTimeMillis()
)
