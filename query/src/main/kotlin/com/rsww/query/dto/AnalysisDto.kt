package com.rsww.query.dto

import com.rsww.shared.utils.AnalysisStatus

data class AnalysisDto(

    var id: Long = 0L,

    var fileId: Long = 0L,

    var fileName: String = "",

    @AnalysisStatus
    var status: String = AnalysisStatus.PENDING
)
