package com.rsww.upload

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
class UploadApplication

fun main(args: Array<String>) {
	runApplication<UploadApplication>(*args)
}
