package com.rsww.upload.utils

import io.micrometer.core.instrument.util.IOUtils
import org.jboss.logging.Logger
import org.springframework.web.multipart.MultipartFile
import java.io.*
import java.util.*


class CleanFileData private constructor(
    private val file: MultipartFile
) {

  companion object {

    fun fromFile(file: MultipartFile) =
        CleanFileData(file)
  }

  fun data(): ByteArray {

    val fileText = readText()

    val cleanText = removeComments(fileText)

    return cleanText.toByteArray()
  }

  private fun readText(): String {
    val byteStream = ByteArrayInputStream(file.bytes)
    return IOUtils.toString(byteStream)
  }

  /**
   * Source:
   * https://stackoverflow.com/questions/8626866/java-removing-comments-from-string/20422698
   */
  private fun removeComments(code: String): String {
    val outsideComment = 0
    val insideLineComment = 1
    val insideBlockComment = 2
    val insideBlockCommentNoNewLineYet = 3
    var currentState = outsideComment
    var endResult = ""
    val s = Scanner(code)
    s.useDelimiter("")
    while (s.hasNext()) {
      val c: String = s.next()
      when (currentState) {
        outsideComment -> if (c == "/" && s.hasNext()) {
          when (val c2 = s.next()) {
            "/" -> currentState = insideLineComment
            "*" -> {
              currentState = insideBlockCommentNoNewLineYet
            }
            else -> endResult += c + c2
          }
        } else endResult += c
        insideLineComment -> if (c == "\n") {
          currentState = outsideComment
          endResult += "\n"
        }
        insideBlockCommentNoNewLineYet -> {
          if (c == "\n") {
            endResult += "\n"
            currentState = insideBlockComment
          }
          while (c == "*" && s.hasNext()) {
            val c2: String = s.next()
            if (c2 == "/") {
              currentState = outsideComment
              break
            }
          }
        }
        insideBlockComment -> while (c == "*" && s.hasNext()) {
          val c2: String = s.next()
          if (c2 == "/") {
            currentState = outsideComment
            break
          }
        }
      }
    }
    s.close()
    return endResult
  }
}
