package com.rsww.upload.controller

import com.rsww.upload.service.StorageService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.ws.rs.PathParam

@RestController
@RequestMapping("/upload")
class UploadController @Autowired constructor(
    private val storageService: StorageService
) {

  private val logger = LoggerFactory.getLogger(UploadController::class.java)

  @PostMapping("/file")
  fun uploadFile(@RequestParam("file") file: MultipartFile): ResponseEntity<String> =
      try {
        storageService.store(file, false)

        ResponseEntity("Upload successful", HttpStatus.OK)
      } catch (e: Exception) {

        ResponseEntity(e.message, HttpStatus.EXPECTATION_FAILED)
      }

  @PostMapping("/template")
  fun uploadTemplate(@RequestParam("file") file: MultipartFile): ResponseEntity<String> =
      try {
        storageService.store(file, true)

        ResponseEntity("Upload successful", HttpStatus.OK)
      } catch (e: Exception) {

        ResponseEntity(e.message, HttpStatus.EXPECTATION_FAILED)
      }

  @PatchMapping("/make-template/{id}")
  fun makeFileTemplate(@PathVariable("id") id: Long): ResponseEntity<String> {

    val success = storageService.makeFileTemplate(id)

    return if (success) {
      ResponseEntity("Successfully set file as template.", HttpStatus.OK)
    } else {
      ResponseEntity("File does not exist", HttpStatus.CONFLICT)
    }
  }
}
