package com.rsww.upload.service

import com.rsww.upload.entity.FileEntity
import com.rsww.upload.repository.FileRepository
import com.rsww.upload.utils.CleanFileData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import org.springframework.web.multipart.MultipartFile

@Service
class StorageService @Autowired constructor(
    private val fileRepository: FileRepository
) {

  fun store(file: MultipartFile, isTemplate: Boolean) {

    val originalName = StringUtils.cleanPath(file.originalFilename.orEmpty())
    val type = file.contentType.orEmpty()
    val data = CleanFileData.fromFile(file).data()

    val entity = FileEntity(
        name = originalName,
        type = type,
        data = data,
        isTemplate = isTemplate
    )

    fileRepository.save(entity)
  }

  fun makeFileTemplate(id: Long): Boolean {

    val file = fileRepository.findById(id)

    return if (file.isPresent) {
      val entity = file.get()
      entity.isTemplate = true
      fileRepository.save(entity)
      true
    } else false
  }
}
