from json import dumps
from locust import HttpUser, task
import os
import copy

class TestUser(HttpUser):
    def on_start(self):

        self.client.headers["Content-Type"] = "application/json"
        self.create_users()
        self.login()
        self.register("new_user_12345")


        self.template_upload_limit = 100
        self.file_upload_limit = 50

        self.template_id = 0
        self.file_id = 0
        self.analysis_file_id = 0

        self.files_dir = os.path.join(os.getcwd(), "data")
        self.filenames = os.listdir(self.files_dir)

    
    def create_users(self):
        for i in range(1000):
            self.register(f"user_{i}")

    def register(self, username):

        body = {
                "username": username,
                "password": "password"
            }
        self.client.request("POST", "/auth/register", data=dumps(body), headers=self.client.headers)

    def login(self):

        body = {
                "username": "validuser",
                "password": "password"
            }
        response = self.client.request("POST", "/auth/login", data=dumps(body), headers=self.client.headers)
        if response.status_code == 200:
            self.client.headers["Authorization"] = f"Bearer {response.text}"


    @task(100)
    def upload_template_file(self):
        if self.template_id < self.template_upload_limit:

            headers = dict()

            headers["Authorization"] = self.client.headers["Authorization"]
            headers["Content-Type"] = None

            filename = self.filenames[self.template_id]
            path = os.path.join(self.files_dir, filename)
            
            files = {
                "file": (filename, open(path, 'rb'), "text/javascript")
                }
            self.client.request("POST", "/upload/template", files=files, headers=headers)
            self.template_id = self.template_id + 1

    @task(50)
    def upload_file(self):
        if self.file_id < self.file_upload_limit:

            headers = dict()

            headers["Authorization"] = self.client.headers["Authorization"]
            headers["Content-Type"] = None

            filename = self.filenames[self.file_id + 100]
            path = os.path.join(self.files_dir, filename)
            
            files = {
                "file": (filename, open(path, 'rb'), "text/javascript")
                }
            self.client.request("POST", "/upload/file", files=files, headers=headers)
            self.file_id = self.file_id + 1

    @task(1000)
    def list_of_files(self):
        self.client.request("GET", "/download/file", headers=self.client.headers)

    @task(1000)
    def list_of_templates(self):
        self.client.request("GET", "/download/template", headers=self.client.headers)

    @task(2000)
    def start_analysis(self):
        response = self.client.post(f"/document/start-analysis/1")
        self.analysis_file_id = self.analysis_file_id + 1

    @task(2000)
    def list_of_statuses(self):
        self.client.request("GET", "/document/analysis", headers=self.client.headers)
